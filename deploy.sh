#!/bin/bash

set -ex

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"

jekyll build
rsync -avh --delete _site/ /var/www/freeablo.org/
