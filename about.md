---
layout: page
title: About
permalink: /about/
---

Hello my friend, stay a while and listen!

freeablo is a modern cross platform reimplementation of the game engine used in Diablo 1. As it is just an engine, you will need the original data files to play the game.

The idea is that it replaces Diablo.exe, and allows you to play the game in proper widescreen resolutions, with zoom and GUI scaling, and a modern modding API, on modern versions of Windows, GNU/Linux and MacOS.
It is a work in progress, so stay tuned if you're interested.
