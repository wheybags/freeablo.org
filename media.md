---
layout: page
title: Media
permalink: /media/
---

v0.3 release video:
<div class="aspect-ratio">
<iframe src="//www.youtube.com/embed/AwGktV5_N4g" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div>

v0.2 release video:
<div class="aspect-ratio">
<iframe src="//www.youtube.com/embed/qA3SrOkJzgw" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div>

Messing around in v0.1:
<div class="aspect-ratio">
<iframe src="//www.youtube.com/embed/QFrU1szmPZk" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div>
