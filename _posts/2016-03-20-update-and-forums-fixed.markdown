---
layout: post
title: "Update + forums fixed"
date: 2016-03-20
---

Hey, so not much has been happening in the past month or so because work was pretty hectic for me and I had no free time to work on freeablo.
The good news is that has changed. MP is pretty close to done, expect a proper video of that soon.
Also, the forums were absolutely inundated with spam, that's been fixed now, and hopefully won't happen again, as I've enabled captchas for signup.
o/
