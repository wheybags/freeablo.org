---
layout: post
title: "More multiplayer progress"
date: 2016-10-15
---

You can now hit things until they fall over, even in multiplayer!

Also, the multiplayer code has been massively overhauled, so things are in a much better state for moving forward.

Anyway, here's a quick video of some things being hit until they fall over:

<div class="aspect-ratio">
<iframe src="//www.youtube.com/embed/1Ol9osASjWw" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div>
