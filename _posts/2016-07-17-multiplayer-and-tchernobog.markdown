---
layout: post
title: "Multiplayer progress + Tchernobog"
date: 2016-07-17
---

Hi all,

There's been some progress on multiplayer, I've been working to switch to a unit tested serialisation framework based on the gafferongames article series <a href="http://gafferongames.com/building-a-game-network-protocol/">here</a>.

So far most stuff is working, with the notable exception of monsters in the dungeon. Below is a short video demo:

<div class="aspect-ratio">
<iframe src="//www.youtube.com/embed/t5geJonvZFQ" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div>

&nbsp;

Also, another big announcement, following <a href="https://freeablo.org/forum/viewtopic.php?f=4&amp;t=27">this forum post</a>, brightlord, the developer behind the Diablo 1 HD mod/Tchernobog has shared the sources for Tchernobog with me to use as a resource for the further development of freeablo. So, a big thanks to him for that, and here's hoping it'll come in useful in the future. I'm afraid, however, I can't share the sources at the moment, but chunks of them will probably end up in the freeablo codebase.

