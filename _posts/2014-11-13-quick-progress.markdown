---
layout: post
title: "Quick progress report"
date: 2014-11-13
---

So, it was called to my attention that I haven't updated this site in a good while.
Since the last update, there's been a few changes - the big ones being gui and level generation for dungeon levels 5-8 (still in a pr, [https://github.com/wheybags/freeablo/pull/93](https://github.com/wheybags/freeablo/pull/93))

Dungeon level 5:

<a href="{{ "assets/screenshots/Screenshot-from-2014-11-13-151043.jpg" | relative_url }}"><img src="{{ "assets/screenshots/Screenshot-from-2014-11-13-151043.jpg" | relative_url }}" width="300" height="160"></a>

Main menu:

<a href="{{ "assets/screenshots/Screenshot-from-2014-11-13-151643.png" | relative_url }}"><img src="{{ "assets/screenshots/Screenshot-from-2014-11-13-151643.png" | relative_url }}" width="300" height="225"></a>

Pause menu:

<a href="{{ "assets/screenshots/Screenshot-from-2014-11-13-151652.png" | relative_url }}"><img src="{{ "assets/screenshots/Screenshot-from-2014-11-13-151652.png" | relative_url }}" width="300" height="223" /></a>

Char and Inv windows (don't actually do anything yet):

<a href="{{ "assets/screenshots/Screenshot-from-2014-11-13-152139.png" | relative_url }}"><img src="{{ "assets/screenshots/Screenshot-from-2014-11-13-152139.png" | relative_url }}" width="300" height="226" /></a>



As you can probably see, the gui bits are fairly placeholder for now, missing some visual features (like trasparency for that ugly green, and animation for the spinning pentagrams and fire), but they are a good base in my humble opinion :P



Anyway, development does continue, in fact we're probably approaching a point where we can release v0.2, if you take a look at the github milestone: <a href="https://github.com/wheybags/freeablo/milestones/v0.2">https://github.com/wheybags/freeablo/milestones/v0.2</a>, it's mostly finished, with music being the only big feature remaining.

