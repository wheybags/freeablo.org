---
layout: page
title: Downloads
permalink: /downloads/
---

## v0.4 (latest)
Windows: [freeablo\_v0.4\_windows.zip](https://github.com/wheybags/freeablo/releases/download/v0.4/freeablo_v0.4_windows.zip)
<br>
Linux: [freeablo\_v0.4\_linux.zip](https://github.com/wheybags/freeablo/releases/download/v0.4/freeablo_v0.4_linux.zip)
<br>
Osx: [freeablo\_v0.4\_osx.zip](https://github.com/wheybags/freeablo/releases/download/v0.4/freeablo_v0.4_osx.zip)

## v0.3
Windows: [freeablo-v0.3-win32.zip](https://github.com/wheybags/freeablo/releases/download/v0.3/freeablo-v0.3-win32.zip)
<br>
Linux: [freeablo-v0.3-linux-64.zip](https://github.com/wheybags/freeablo/releases/download/v0.3/freeablo-v0.3-linux-64.zip)
<br>
Osx: [freeablo-v0.3-osx.zip](https://github.com/wheybags/freeablo/releases/download/v0.3/freeablo-v0.3-osx.zip)

## v0.2
Windows: [freeablo-v0.2-win32.zip](https://github.com/wheybags/freeablo/releases/download/v0.2/freeablo-v0.2-win32.zip)
<br>
Linux: [freeablo-v0.2-linux-64.zip](https://github.com/wheybags/freeablo/releases/download/v0.2/freeablo-v0.2-linux-64.zip)
<br>
Osx: [freeablo-v0.2-osx.zip](https://github.com/wheybags/freeablo/releases/download/v0.2/freeablo-v0.2-osx.zip)

## v0.1
Windows: [freeablo-v0.1-win32.zip](https://github.com/wheybags/freeablo/releases/download/cdv0.1/freeablo-v0.1-win32.zip)
<br>
Linux: [freeablo-v0.1-linux-64.zip](https://github.com/wheybags/freeablo/releases/download/cdv0.1/freeablo-v0.1-linux-64.zip)
<br>
Osx: [freeablo-v0.1-linux-64.zip](https://github.com/wheybags/freeablo/releases/download/cdv0.1/freeablo-v0.1-linux-64.zip)
